from sklearn.datasets import load_iris
import matplotlib.pyplot as plt
from itertools import combinations
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score


def projection(X, y):
    fig, axs = plt.subplots(2,3)
    fig.set_size_inches(20, 10)
    comb = list(combinations((0,1,2,3), 2))
    col_names = ["sepal length", "sepal width", "petal length", "petal width"]
    for i in range(2):
        for j in range(3):
            axs[i][j].scatter(X[:,comb[i*3+j][0]], X[:,comb[i*3+j][1]],
                             c = y)
            axs[i][j].set_title(f"Projection on {col_names[comb[i*3+j][0]]} and {col_names[comb[i*3+j][1]]}")
            axs[i][j].set_xlabel(col_names[comb[i*3+j][0]])
            axs[i][j].set_ylabel(col_names[comb[i*3+j][1]])
    plt.show()


X = load_iris()['data']
y = load_iris()['target']
projection(X, y)

X.mean(axis=0)

X.std(axis=0)

class Z_Normalizer:
    def __init__(self):
        self.mean = 0
        self.std = 1

    def fit(self, X):
        self.std = X.std(axis=0)
        self.mean = X.mean(axis=0)

    def transform(self, X):
        return (X-self.mean)/self.std

    def fit_transform(self, X):
        self.fit(X)
        return self.transform(X)


normalizer = Z_Normalizer()
X_ = normalizer.fit_transform(X)

projection(X_, y)


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0., random_state=10)


best_model = None
best_accuracy = 0
best_k = -1

X_train_transformed = normalizer.fit_transform(X_train)
X_test_transformed = normalizer.transform(X_test)

for k in range(3, 15):
    curr_model = KNeighborsClassifier(n_neighbors=k)
    curr_model.fit(X_train_transformed, y_train)
    preds = curr_model.predict(X_test_transformed)
    acc = accuracy_score(y_test, preds)
    if acc > best_accuracy:
        best_model = curr_model
        best_accuracy = acc
        best_k = k

print(best_accuracy, best_k)

n = int(input("Скольким объектам вы хотите определить класс?\t"))
new_data = np.array([list(map(float, input("Введите значения характеристик (по 4 float через пробел):\t").split())) for _ in range(n)])

print(new_data)

norm_new_data = normalizer.transform(new_data)
print(norm_new_data)

for inp, prd in zip(new_data, best_model.predict(norm_new_data)):
    print(f'Class for iris {inp} is {prd}')
