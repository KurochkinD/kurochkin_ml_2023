import pygame
import numpy as np
import random
import seaborn as sns


def dist(pointA, pointB):
    return np.sqrt(
        (pointA[0] - pointB[0]) * (pointA[0] - pointB[0]) + (pointA[1] - pointB[1]) * (pointA[1] - pointB[1]))


def generate_random_points(coord):
    count = random.randint(2, 5)
    points = []
    for i in range(count):
        angle = np.pi * random.randint(0, 360) / 180
        radius = random.randint(10, 20)
        x = radius * np.cos(angle) + coord[0]
        y = radius * np.sin(angle) + coord[1]
        points.append((x, y))
    return points


def draw_points(screen, points, colors):
    screen.fill(color='#FFFFFF')
    for point, clr in zip(points, colors):
        pygame.draw.circle(screen, color=clr, center=point, radius=r)


def find_neighbors(data, point_index, e):
    return [i for i, point in enumerate(data) if dist(data[point_index], point) <= e]


def add_to_cluster(data, labels, point_index, neighbors, cluster_label, e, min_samples):
    labels[point_index] = cluster_label

    i = 0
    while i < len(neighbors):
        neighbor_index = neighbors[i]

        if labels[neighbor_index] == 0:
            labels[neighbor_index] = cluster_label
            neighbor_neighbors = find_neighbors(data, neighbor_index, e)

            if len(neighbor_neighbors) >= min_samples:
                neighbors.extend(neighbor_neighbors)

        i += 1


def dbscan(data, e, min_samples, screen, points, colors):
    labels = [0] * len(data)
    cluster_label = 0

    for i, point in enumerate(data):
        if labels[i] != 0:
            continue

        neighbors = find_neighbors(data, i, e)

        if len(neighbors) >= min_samples:
            colors[i] = '#00FF00'
    draw_points(screen, points, colors)
    pygame.display.update()

    for i, point in enumerate(data):
        if labels[i] != 0:
            continue

        neighbors = find_neighbors(data, i, e)

        if len(neighbors) < min_samples:
            colors[i] = '#FF0000'
            for j in neighbors:
                if colors[j] == '#00FF00':
                    colors[i] = '#FFFF00'
    draw_points(screen, points, colors)
    pygame.display.update()

    for i, point in enumerate(data):
        if labels[i] != 0:
            continue

        neighbors = find_neighbors(data, i, e)

        if len(neighbors) < min_samples:
            labels[i] = -1
        else:
            cluster_label += 1
            add_to_cluster(data, labels, i, neighbors, cluster_label, e, min_samples)

    return labels, cluster_label + 1


if __name__ == '__main__':
    r = 3
    pygame.init()
    screen = pygame.display.set_mode((900, 600), pygame.RESIZABLE)
    screen.fill(color='#FFFFFF')
    pygame.display.update()
    is_pressed = False
    points = []
    colors = []
    e = 25
    min_samples = 3
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
            if event.type == pygame.VIDEORESIZE:
                draw_points(screen, points, colors)
            print(event)
            if event.type == pygame.KEYUP:
                if event.key == 8:
                    screen.fill(color='#FFFFFF')
                    points = []
                    colors = []
                elif event.key == 13:
                    labels, labels_n = dbscan(points, e, min_samples, screen, points, colors)

                    palette = sns.color_palette("Set1", n_colors=labels_n)
                    distinct_colors = list(palette.as_hex())
                    colors = [distinct_colors[label] for label in labels]
                    draw_points(screen, points, colors)

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    is_pressed = True
            if event.type == pygame.MOUSEBUTTONUP:
                is_pressed = False
            if is_pressed:
                coord = event.pos
                if len(points):
                    if dist(points[-1], coord) > 5 * r:
                        pygame.draw.circle(screen, color='#FF0000',
                                           center=coord, radius=r)
                        near_point = generate_random_points(coord)
                        clrs = ['#FF0000'] * len(near_point)
                        points.extend(near_point)
                        colors.extend(clrs)
                        for elem in near_point:
                            pygame.draw.circle(screen, color='#FF0000',
                                               center=elem, radius=r)
                        points.append(coord)
                        colors.append('#FF0000')
                else:
                    pygame.draw.circle(screen, color='#FF0000',
                                       center=coord, radius=r)
                    points.append(coord)
                    colors.append('#FF0000')

            pygame.display.update()
