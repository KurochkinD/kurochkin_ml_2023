import networkx as nx
import matplotlib.pyplot as plt
import random
import numpy as np


def generate_random_weighted_graph(n_nodes=15, density=0.2):
    gr = nx.Graph()
    n_nodes += 1
    gr.add_nodes_from(range(1, n_nodes))

    for i in range(1, n_nodes):
        for j in range(i + 1, n_nodes):
            if random.random() < density:
                weight = random.randint(1, 99)
                gr.add_edge(i, j, weight=weight)

    if not nx.is_connected(gr):
        print("Граф не связный, генерирую заново...")
        gr = generate_random_weighted_graph(n_nodes, density)

    return gr

graph = generate_random_weighted_graph()


def print_adjacency_matrix(graph):
    num_nodes = len(graph.nodes)
    print('\t|', end='')
    for i in range(num_nodes):
        print(f'{i+1}\t', end='|')
    print()

    print('-' * (num_nodes*4 + 5))

    for i in range(num_nodes):
        print(f'{i+1}\t|', end='')
        for j in range(num_nodes):
            if i == j:
                print('0\t|', end='')
            elif graph.has_edge(i, j):
                print(graph[i][j]['weight'], end='\t|')
            else:
                print('-\t|', end='')
        print()


print_adjacency_matrix(graph)


def draw_graph(graph):
    pos = nx.spring_layout(graph)
    nx.draw(graph, pos, with_labels=True, node_color='lightgreen', node_size=700, font_size=9, font_color='black')
    labels = nx.get_edge_attributes(graph, 'weight')
    nx.draw_networkx_edge_labels(graph, pos, edge_labels=labels)
    plt.show()


draw_graph(graph)


def prim_minimum_spanning_tree(graph):
    mst = nx.Graph()
    nodes = list(graph.nodes)
    start_node = nodes[0]
    visited = set([start_node])

    while len(visited) < len(nodes):
        possible_edges = []
        for node in visited:
            possible_edges.extend([
                (node, neighbor, data['weight']) for neighbor, data in graph[node].items() if neighbor not in visited
            ])
        if not possible_edges:
            break
        min_edge = min(possible_edges, key=lambda x: x[2])
        mst.add_edge(min_edge[0], min_edge[1], weight=min_edge[2])
        visited.add(min_edge[1])

    return mst


mst = prim_minimum_spanning_tree(graph)
draw_graph(mst)


def divide_tree_into_clusters(tree, k=5):
    edges = list(tree.edges(data=True))
    edges.sort(key=lambda x: x[2]['weight'])

    removed_edges = edges[-(k-1):]
    for edge in removed_edges:
        tree.remove_edge(edge[0], edge[1])

    clusters = list(nx.connected_components(tree))

    return clusters


def draw_graph_with_clusters(graph, clusters):
    pos = nx.spring_layout(graph)
    colors = plt.cm.viridis(np.linspace(0.3, 1, len(clusters)))
    for i, cluster in enumerate(clusters):
        subgraph = graph.subgraph(cluster)
        nx.draw(subgraph, pos, with_labels=True, node_color=[colors[i]]*len(subgraph.nodes), node_size=1000, font_size=10, font_color='black')
        labels = nx.get_edge_attributes(subgraph, 'weight')
        nx.draw_networkx_edge_labels(subgraph, pos, edge_labels=labels)

    plt.show()


clusters = divide_tree_into_clusters(mst)
draw_graph_with_clusters(mst, clusters)
